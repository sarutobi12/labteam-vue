import Vue from "vue";
import VueRouter from "vue-router";
//admin
import OC from "./views/admin/oc/Index.vue";
import Project from "./views/admin/project/Index.vue";
import Role from "./views/admin/role/Index.vue";
import User from "./views/admin/user/Index.vue";

//client
import Task from "./views/clients/Task.vue";
import History from "./views/clients/History.vue";

// auth
import Auth from "./views/shares/auth/Auth.vue";
import Login from "./views/shares/auth/Login.vue";

//dash
import Dash from "./views/shares/dash/Dash.vue";
import Home from "./views/shares/dash/Home.vue";


//upload

import Article from "./views/admin/article/Index"

//Slide
import Slide from "./views/admin/slide/Index"

//Traning Aerial
import Training from "./views/admin/training/Index"

//OKR
import OKR from "./views/admin/okr/Index"

Vue.use(VueRouter);
const router = new VueRouter({
  // mode: 'history',
  routes: [
    {
      //HOME
      path: "/",
      component: Home,
      redirect: "/home",
      children: [{ path: "home", component: Home }]
    },
    {
      //HOME
      path: "/portfolio",
      component: Home,
      redirect: "/home",
      children: [{ path: "home", component: Home }]
    },
    //admin
    {
      path: "/admin",
      component: Dash,
      children: [{ path: "/admin", component: Project ,meta:{ requiresAuth: true} }]
    },
    //adminoc
    {
      path: "/admin-oc",
      component: Dash,
      children: [{ path: "/admin-oc", component: OC }]
    },
    //adminrole
    {
      path: "/admin-role",
      component: Dash,
      children: [{ path: "/admin-role", component: Role }]
    },
    //adminuser
    {
      path: "/admin-user",
      component: Dash,
      children: [{ path: "/admin-user", component: User }]
    },
    //adminproject
    {
      path: "/admin-project",
      component: Dash,
      children: [{ path: "/admin-project", component: Project }]
    },
    //adminSlide
    {
      path: "/admin-slide",
      component: Dash,
      children: [{ path: "/admin-slide", component: Slide }]
    },
    
    //Login
    {
      path: "/login",
      component: Auth,
      children: [{ path: "/login", component: Login}]
    },

    //client task
    {
      path: "/client-task",
      component: Dash,
      children: [{ path: "/client-task", component: Task }]
    },

    //client task
    {
      path: "/client-history",
      component: Dash,
      children: [{ path: "/client-history", component: History  }]
    },
    //client task
    {
      path: "/article",
      component: Dash,
      children: [{ path: "/article", component: Article  }]
    },

    //Training Aerial
    {
      path: "/traning-resources",
      component: Dash,
      children: [{ path: "/traning-resources", component: Training  }]
    },
    //OKR
    {
      path: "/okr",
      component: Dash,
      children: [{ path: "/okr", component: OKR  }]
    }
  ]
});

export default router;
